Customer Manager

> Install requirements 
  `pip3 freeze > requirements.txt`

> Navigate outside the project directory 
  `cd ..`

> Set environmental variables 
   `export FLASK_APP=customer_manager`

> Set debug mode (optional) 
   `export FLASK_DEBUG=1`

> Run application 
 ` flask Run`
  

  
