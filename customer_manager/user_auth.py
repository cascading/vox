from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required


from .models import User
from . import db

user_auth = Blueprint('user_auth', __name__)


@user_auth.route('/auth_login')
def get_login():
    return render_template('auth/login.html')


@user_auth.route('/signup')
def get_signup():
    return render_template('auth/signup.html')


@user_auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@user_auth.route('/login', methods=['POST'])
def post_login():
    email = request.form.get('email')
    password = request.form.get('password')
    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('user_auth.get_login'))

    login_user(user)
    return redirect(url_for('main.customers'))


@user_auth.route('/signup', methods=['POST'])
def post_signup():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(
        email=email).first()  # return the user if one exists. There should only be one because the email is unique

    # check if user exists, display error msg if the users exists and let the user try again
    if user:
        flash('Email address already exists')
        return redirect(url_for('user_auth.get_signup'))

    # we hash the password here even though its not a requirement for the assessment
    new_user = User(name=name, password=generate_password_hash(password, method='sha256'), is_active=True)

    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('user_auth.get_login'))  # redirect to login screen
