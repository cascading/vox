from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager


# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = '64b186dad5ba4fb4bcc86441c2d18efc'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///customer_manager_db.sqlite'

    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'user_auth.get_login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    # blueprint for user_auth routes in our app
    from .user_auth import user_auth as user_auth_blueprint
    app.register_blueprint(user_auth_blueprint)

    # blueprint for non-user_auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
