from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_required, current_user

from .models import Customer
from . import db

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('auth/index.html')


@main.route('/customers')
@login_required
def customers():
    return render_template('auth/customer.html', name=current_user.name)


@main.route('/customers/new',  methods=['GET'])
@login_required
def get_customer():
    return render_template('customers/create.html')


@main.route('/customers/new',  methods=['POST'])
@login_required
def create_customer():
    email = request.form.get('email')
    company = request.form.get('company_name')
    contact_number = request.form.get('contact_number')

    customer = Customer.query.filter_by(
        email=email).first()  # return the customer if one exists. There should only be one because the email is unique

    if customer:
        flash('Email address already exists')
        return redirect(url_for('user_auth.customers'))

    new_customer = Customer(
        email=email,
        company_name=company,
        contact_number=contact_number
    )

    db.session.add(new_customer)
    db.session.commit()

    return redirect(url_for('main.customers'))  # redirect to customer screen


@main.route('/customers/list',  methods=['GET'])
@login_required
def display_customers():
    customers = Customer.query.all()
    return render_template('customers/list.html', customers=customers)


@main.route('/customers/find')
@login_required
def find_customer():
    customer_id = request.args.get("customer", None)
    customer = Customer.query.filter_by(id=customer_id).first()
    if customer:
        return render_template('customers/modify.html', customer=customer)
    return render_template('customers/search.html')


@main.route('/customers/update',  methods=['POST'])
@login_required
def update_customer():

    customer_id = request.form.get('cs_id')
    email = request.form.get('email')
    company = request.form.get('company_name')
    contact_number = request.form.get('contact_number')

    customer = Customer.query.filter_by(id=customer_id).first()
    customer.email = email
    customer.company_name = company
    customer.contact_number = contact_number

    db.session.commit()
    return redirect(url_for('main.customers'))  # redirect to customer screen